package by.De_SckapG.gmbusiness.objects.business;

import by.De_SckapG.gmbusiness.objects.Worker;
import by.De_SckapG.gmbusiness.objects.economy.AEconomy;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.List;

public abstract class ABusiness {

	protected List<Worker> workers;
	protected int id, cost, percent, balance, taxBalance, tax;
	protected String owner, warp, name;
	protected AEconomy economy;
	protected boolean isClosed;
	protected Location location;

	protected ABusiness(List<Worker> workers, int id, int cost, int percent, int balance, int taxBalance,
						int tax, String owner, String warp, String name, AEconomy economy, boolean isClosed, Location location) {
		this.workers = workers;
		this.id = id;
		this.cost = cost;
		this.percent = percent;
		this.balance = balance;
		this.taxBalance = taxBalance;
		this.tax = tax;
		this.owner = owner;
		this.warp = warp;
		this.name = name;
		this.economy = economy;
		this.isClosed = isClosed;
		this.location = location;
	}

	public String getOwner() {
		return owner;
	}

	public int getId() {
		return id;
	}

	public List<Worker> getWorkers() {
		return workers;
	}

	public int getCost() {
		return cost;
	}

	public int getPercent() {
		return percent;
	}

	public boolean isClosed() {
		return isClosed;
	}

	public int getBalance() {
		return balance;
	}

	public String getWarp() {
		return warp;
	}

	public AEconomy getEconomy() {
		return economy;
	}

	public Location getLocation() {
		return location;
	}

	public int getTax() {
		return tax;
	}

	public int getTaxBalance() {
		return taxBalance;
	}

	public String getName() {
		return name;
	}

	public abstract void openInventory(Player player);

	public abstract String getType();

	public abstract Material getIcon();

	public abstract String getIconDynMap();
}
