package by.De_SckapG.gmbusiness.objects.business;

import by.De_SckapG.gmbusiness.Utils;
import by.De_SckapG.gmbusiness.objects.Worker;
import by.De_SckapG.gmbusiness.objects.economy.AEconomy;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.List;

public final class Tele extends ABusiness {

	protected Tele(List<Worker> workers, int id, int cost, int percent, int balance, int taxBalance, int tax, String owner, String warp, String name, AEconomy economy, boolean isClosed, Location location) {
		super(workers, id, cost, percent, balance, taxBalance, tax, owner, warp, name, economy, isClosed, location);
	}

	@Override
	public void openInventory(Player player) {
		Inventory inventory = Bukkit.createInventory(null, 9, "�l������ " + super.getId());
		inventory.addItem(
			Utils.buildItem(Material.STAINED_GLASS_PANE, 5, 1, "�a����� �������� � ������ SIM-�����.",
					new String[] { "    ", "�8���������: �a" + Utils.getIntWithPercent(250, super.getPercent()) + "$" }),
			Utils.buildItem(Material.STAINED_GLASS_PANE, 5, 1, "�a����� �������� � ������ SIM-�����.",
					new String[] { "    ", "�8���������: �a" + Utils.getIntWithPercent(250, super.getPercent()) + "$" }),
			Utils.buildItem(Material.STAINED_GLASS_PANE, 5, 1, "�a����� �������� � ������ SIM-�����.",
					new String[] { "    ", "�8���������: �a" + Utils.getIntWithPercent(250, super.getPercent()) + "$" }),
			Utils.buildItem(Material.STAINED_GLASS_PANE, 5, 1, "�a����� �������� � ������ SIM-�����.",
					new String[] { "    ", "�8���������: �a" + Utils.getIntWithPercent(250, super.getPercent()) + "$" }),
			Utils.buildItem(Material.NETHER_STAR, 0, 1, "�a" + super.getName(),
					new String[]{"     ", "�7���: �e" + getType(),
							!this.getOwner().equals("NULL") ? "�7��������: �a" + this.getOwner() : "�7��������: �c���",
							"�7��� ����������� �e��������",  "  ",
							"�7������:",
							" �7������: �a"+Utils.getIntWithPercent(5, super.getPercent())+"$/������",
							" �7SMS: �a"+Utils.getIntWithPercent(10, super.getPercent())+"$", "    "}),
			Utils.buildItem(Material.STAINED_GLASS_PANE, 14, 1, "�c����������", null),
			Utils.buildItem(Material.STAINED_GLASS_PANE, 14, 1, "�c����������", null),
			Utils.buildItem(Material.STAINED_GLASS_PANE, 14, 1, "�c����������", null),
			Utils.buildItem(Material.STAINED_GLASS_PANE, 14, 1, "�c����������", null)
		);
		player.openInventory(inventory);
	}

	@Override
	public String getType() {
		return "�������� �����";
	}

	@Override
	public Material getIcon() {
		return Material.CLAY_BRICK;
	}

	@Override
	public String getIconDynMap() {
		return "cart";
	}
}
