package by.De_SckapG.gmbusiness.objects.business;

import by.De_SckapG.gmbusiness.Utils;
import by.De_SckapG.gmbusiness.objects.Worker;
import by.De_SckapG.gmbusiness.objects.economy.AEconomy;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.List;

public final class Cafe extends ABusiness {

	protected Cafe(List<Worker> workers, int id, int cost, int percent, int balance, int taxBalance, int tax, String owner, String warp, String name, AEconomy economy, boolean isClosed, Location location) {
		super(workers, id, cost, percent, balance, taxBalance, tax, owner, warp, name, economy, isClosed, location);
	}

	@Override
	public void openInventory(Player player) {
		Inventory inventory = Bukkit.createInventory(null, 9, "�l������ " + super.getId());
		inventory.addItem(
				Utils.buildItem(Material.BREAD, 0, 1, "�b�l������� �����",
						new String[]{" ",
								"�8���������: �a" + Utils.getIntWithPercent(115, super.getPercent()) + "$", " "}),
				Utils.buildItem(Material.GRILLED_PORK, 0, 1, "�b�l���������� �������",
						new String[]{" ",
								"�8���������: �a" + Utils.getIntWithPercent(135, super.getPercent()) + "$", " "}),
				Utils.buildItem(Material.COOKED_BEEF, 0, 1, "�b�l���������� �����",
						new String[]{" ",
								"�8���������: �a" + Utils.getIntWithPercent(140, super.getPercent()) + "$", " "}),
				Utils.buildItem(Material.COOKED_FISH, 0, 1, "�b�l���������� ����",
						new String[]{" ",
								"�8���������: �a" + Utils.getIntWithPercent(130, super.getPercent()) + "$", " "}),
				Utils.buildItem(Material.COOKED_CHICKEN, 0, 1, "�b�l���������� ������",
						new String[]{" ",
								"�8���������: �a" + Utils.getIntWithPercent(120, super.getPercent()) + "$", " "}),
				Utils.buildItem(Material.MUSHROOM_SOUP, 0, 1, "�b�l���",
						new String[]{" ",
								"�8���������: �a" + Utils.getIntWithPercent(125, super.getPercent()) + "$", " "}),
				Utils.buildItem(Material.COOKED_MUTTON, 0, 1, "�b�l�������� ��������",
						new String[]{" ",
								"�8���������: �a" + Utils.getIntWithPercent(175, super.getPercent()) + "$", " "})

		);
		inventory.setItem(8, Utils.buildItem(Material.NETHER_STAR, 0, 1, "�a" + super.getName(),
				new String[]{" ", "�7���: �e" + getType(),
						!this.getOwner().equals("NULL") ? "�7��������: �a" + super.getOwner() : "�7��������: �c���", "�7��� ����������� �e��������"}));
		player.openInventory(inventory);
	}

	@Override
	public String getType() {
		return "����";
	}

	@Override
	public Material getIcon() {
		return Material.COOKED_CHICKEN;
	}

	@Override
	public String getIconDynMap() {
		return "cup";
	}
}
