package by.De_SckapG.gmbusiness.objects.business;

import by.De_SckapG.gmbusiness.Utils;
import by.De_SckapG.gmbusiness.objects.Worker;
import by.De_SckapG.gmbusiness.objects.economy.AEconomy;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

import java.util.List;

public final class Standart extends ABusiness {

	protected Standart(List<Worker> workers, int id, int cost, int percent, int balance, int taxBalance, int tax, String owner, String warp, String name, AEconomy economy, boolean isClosed, Location location) {
		super(workers, id, cost, percent, balance, taxBalance, tax, owner, warp, name, economy, isClosed, location);
	}

	@Override
	public void openInventory(Player player) {
		Inventory inventory = Bukkit.createInventory(null, 9, "�l������ " + super.getId());
		inventory.addItem(
				Utils.buildItem(Material.PUMPKIN, 0, 1, "�b�l�����",
						new String[]{"    ", "�8�������� ��� �� 5 �����.",
								"�8���������: �a" + Utils.getIntWithPercent(100, super.getPercent()) + "$", "   "}),
				Utils.buildItem(Material.NETHER_BRICK, 0, 1, "�b�l����� �������",
						new String[]{"    ", "�8��������������� ���� �������� � ������� 2 �����.",
								"�8���������: �a" + Utils.getIntWithPercent(75, super.getPercent()) + "$", "   ",
								"�4(( �c������� � �������� ����� - ���!",
								"�c��� ����� �������� ��������� ������ �������� � ������� �� ������� ������! �4))", "   "}),
				Utils.buildItem(Material.FLINT_AND_STEEL, 0, 1, "�b�l���������",
						new String[]{"    ", "�8����� ����� ��������� ��������.",
								"�8���������: �a" + Utils.getIntWithPercent(85, super.getPercent()) + "$", "   "}),
				Utils.buildItem(Material.BEETROOT_SEEDS, 0, 1, "�b�l��������",
						new String[]{"    ", "�8������� �������, ��������������� ������� ������.",
								"�8���������: �a" + Utils.getIntWithPercent(135, super.getPercent()) + "$", "   "}),
				Utils.buildItem(Material.CLAY_BRICK, 0, 1, "�b�l�������", null),
				Utils.buildItem(Material.IRON_INGOT, 0, 1, "�b�l�������",
						new String[]{"    ", "�8������ ��������������� �����.",
								"�8���������: �a" + Utils.getIntWithPercent(195, super.getPercent()) + "$", "   "}),
				Utils.buildItem(Material.WATCH, 0, 1, "�b�l����",
						new String[]{"    ", "�8���������� �����.",
								"�8���������: �a" + Utils.getIntWithPercent(125, super.getPercent()) + "$", "   "})
		);
		ItemStack newspaper = Utils.getNewspaperItem();
		if (newspaper != null) {
			inventory.addItem(Utils.buildItem(newspaper, "�b�l������",
					new String[]{"    ", "�8" + ((BookMeta) Utils.getNewspaperItem().getItemMeta()).getTitle() + ".",
							"�8���������: �a" + Utils.getIntWithPercent(150, super.getPercent()) + "$", "   "}));
		}
		inventory.setItem(8, Utils.buildItem(Material.NETHER_STAR, 0, 1, "�a" + super.getName(),
				new String[]{" ", "�7���: �e" + getType(),
						!this.getOwner().equals("NULL") ? "�7��������: �a" + super.getOwner() : "�7��������: �c���", "�7��� ����������� �e��������"}));
		player.openInventory(inventory);
	}

	@Override
	public String getType() {
		return "24/7";
	}

	@Override
	public Material getIcon() {
		return Material.TORCH;
	}

	@Override
	public String getIconDynMap() {
		return "cart";
	}
}
