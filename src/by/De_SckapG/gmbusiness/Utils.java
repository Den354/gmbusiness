package by.De_SckapG.gmbusiness;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.Arrays;

public final class Utils {
	public static ItemStack buildItem(Material material, int data, int amount, String name, String[] lore) {
		ItemStack stack = new ItemStack(material, amount, (byte) data);
		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName(name);
		if (lore != null) {
			meta.setLore(Arrays.asList(lore));
		}
		stack.setItemMeta(meta);
		return stack;
	}

	public static ItemStack buildItem(ItemStack stack, String name, String[] lore) {
		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName(name);
		if (lore != null) {
			meta.setLore(Arrays.asList(lore));
		}
		stack.setItemMeta(meta);
		return stack;
	}

	public static ItemStack buildSkull(String owner, int amount, String name, String[] lore) {
		ItemStack stack = new ItemStack(Material.SKULL_ITEM, amount, (byte) 3);
		SkullMeta meta = (SkullMeta) stack.getItemMeta();
		meta.setOwner(owner);
		meta.setDisplayName(name);
		if (lore != null) {
			meta.setLore(Arrays.asList(lore));
		}
		stack.setItemMeta(meta);
		return stack;
	}

	public static ItemStack buildPotion(Material material, int data, int amount, String name, String[] lore, Color cl) {
		ItemStack stack = new ItemStack(material, amount, (short) 0);
		PotionMeta meta = (PotionMeta) stack.getItemMeta();
		meta.setColor(cl);
		meta.setDisplayName(name);
		if (lore != null) {
			meta.setLore(Arrays.asList(lore));
		}
		stack.setItemMeta(meta);
		return stack;
	}

	public static ItemStack getNewspaperItem() {
		return ru.needmine.Utils.Utils.getGazetaItem();
	}

	public static long getIntWithPercent(int x, float percent) {
		if (percent > 1.0) {
			percent /= 100.0;
		}
		return Math.round(x + (1.0 * x * percent));
	}

}
